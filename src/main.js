'use strict';
require('./config');

const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:false}));
const UTIL = require('./util');
const DB_MONGO = require('./db/mongo');


const ENDPOINT_POST_FIND_USER = require('./endpoints/post_api_find_user');
const ENDPOINT_POST_CREATE_USER = require('./endpoints/post_api_insert_invoice');

const BASE_PATH='/api';



app.post(BASE_PATH +'/find_user',ENDPOINT_POST_FIND_USER.main);
app.post(BASE_PATH +'/create_user',ENDPOINT_POST_CREATE_USER.main);


app.use( UTIL.not_found );

DB_MONGO.connect().then(mongo =>{
	app.listen( __.PORT , ()=> console.log( `Listen on PORT ${__.PORT}`) );
})
.catch( error =>{
	console.log(error);
})


